#include <iostream>
#include <fstream>
#include <string>
#include <vector>


bool readFile(std::string filePath, std::vector<int>* inputValues) 
{
	char tempChar;
	std::ifstream inputFile;

	inputFile.open(filePath);
	if (inputFile.is_open())
	{
		while (inputFile.get(tempChar)) 
		{
			inputValues->push_back(tempChar - '0');
		}
		return true;
	}
	else
	{
		return false;
	}
}

int checkSum(std::vector<int>* inputValues , int part)
{
	int runningTotal = 0;
	int offset = 0;
	int inputSize = inputValues->size();
	if (part == 1)
	{
		offset = 1;
	}
	else if (part == 2)
	{
		offset = inputSize / 2;
	}
	for (int i = 0; i < inputSize; ++i)
	{
		if (inputValues->at(i) == inputValues->at((i + offset) % inputSize))
		{
				runningTotal += inputValues->at(i);
		}
	}
	return runningTotal;
}
	

int main()
{
	std::vector<int>* inputValues = new std::vector<int>;
	std::string filePath;

	std::cout << "Enter the file path for the input file" << std::endl;
	std::cin >> filePath;

	if (readFile(filePath, inputValues))
	{
		std::cout << "The answer for part 1 is: " << checkSum(inputValues, 1) << std::endl;
		std::cout << "The answer for part 2 is: " << checkSum(inputValues, 2) << std::endl;
		return 0;
	}
	else
	{
		std::cout << "The file could not be opened" << std::endl;
		return 1;
	}
}

