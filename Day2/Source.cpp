#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

std::vector<std::vector<int>> readFile(std::string filePath)
{
	std::string line;
	std::string value;
	std::ifstream inputFile;
	std::vector<std::vector<int>> inputValues;
	std::vector<int> rowValues;
	inputFile.open(filePath);
	
	while (std::getline(inputFile, line))
	{
		std::stringstream ss(line);

		while (std::getline(ss, value, '	'))
		{
			rowValues.push_back(std::stoi(value));
		}

		inputValues.push_back(rowValues);
		rowValues.clear();
	}
	return inputValues;
}

std::vector<std::vector<int>> sortInputValues(std::vector<std::vector<int>> inputValues)
{
	for (int i=0;i<inputValues.size();++i)
	{
		std::sort(inputValues.at(i).begin(), inputValues.at(i).end());
	}
	return inputValues;
}

int calculateChecksum1(const std::vector<std::vector<int>> &inputValues)
{
	int checkSum = 0;
	for (int i = 0; i < inputValues.size(); ++i)
	{
		checkSum += (inputValues.at(i).back() - inputValues.at(i).front());
	}
	return checkSum;
}
int calculateChecksum2(const std::vector<std::vector<int>> &inputValues)
{
	int checkSum = 0;
	for (int i = 0; i < inputValues.size(); ++i)
	{
		for (int j = 0; j < inputValues.at(i).size(); ++j)
		{
			for (int k = j + 1; k < inputValues.at(i).size(); ++k)
			{
				if ((inputValues.at(i).at(k) % inputValues.at(i).at(j)) == 0)
				{
					checkSum += (inputValues.at(i).at(k) / inputValues.at(i).at(j));
				}
				
			}
		}
	}
	return checkSum;
}
int main()
{
	std::string filePath;
	std::vector<std::vector<int>> inputValues;
	std::vector<std::vector<int>> sortedInputValues;

	std::cout << "Enter the file path for the input file" << std::endl;
	std::cin >> filePath;
	inputValues = readFile(filePath);
	sortedInputValues = sortInputValues(inputValues);
	std::cout << "The check sum for part 1 is: " << calculateChecksum1(sortedInputValues) << std::endl;
	std::cout << "The check sum for part 2 is: " << calculateChecksum2(sortedInputValues) << std::endl;
	return 0;
}